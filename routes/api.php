<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/user', function (Request $request) {
    return $request->user();
});
//slides
Route::get('slides', 'SlideController@index');
Route::post('slides', 'SlideController@save');
Route::get('slides/{id}', 'SlideController@find');
Route::put('slides/{id}', 'SlideController@update');
Route::delete('slides/{id}', 'SlideController@delete');
//abouts
Route::get('abouts/{id}', 'AboutController@find');
Route::put('abouts/{id}', 'AboutController@update');
//services
Route::get('services', 'ServiceController@index');
Route::post('services', 'ServiceController@save');
Route::get('services/{id}', 'ServiceController@find');
Route::put('services/{id}', 'ServiceController@update');
Route::delete('services/{id}', 'ServiceController@delete');
Route::get('services/slug/{slug}', 'ServiceController@slug');
//teams
Route::get('teams', 'TeamController@index');
Route::post('teams', 'TeamController@save');
Route::get('teams/{id}', 'TeamController@find');
Route::put('teams/{id}', 'TeamController@update');
Route::delete('teams/{id}', 'TeamController@delete');
//works
Route::get('works', 'WorkController@index');
Route::post('works', 'WorkController@save');
Route::get('works/{id}', 'WorkController@find');
Route::put('works/{id}', 'WorkController@update');
Route::delete('works/{id}', 'WorkController@delete');
//abouts
Route::get('abouts', 'AboutController@index');
Route::post('abouts', 'AboutController@save');
Route::get('abouts/{id}', 'AboutController@find');
Route::put('abouts/{id}', 'AboutController@update');
//news
Route::get('news', 'NewsController@index');
Route::post('news', 'NewsController@save');
Route::get('news/{id}', 'NewsController@find');
Route::put('news/{id}', 'NewsController@update');
Route::get('news/slug/{slug}', 'NewsController@slug');
Route::delete('news/{id}', 'NewsController@delete');
//upload
Route::post('upload/image', 'UploadController@uploadImage');
Route::post('upload/images', 'UploadController@uploadImages');
Route::post('upload/remove', 'UploadController@removeImage');
//contacts
Route::get('contacts', 'ContactsController@index');
Route::post('contacts', 'ContactsController@save');
Route::get('contacts/{id}', 'ContactsController@find');
Route::put('contacts/{id}', 'ContactsController@update');
//user
Route::post('user/auth', 'UserController@auth');
