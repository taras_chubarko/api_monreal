<?php

use Illuminate\Database\Seeder;

class SlidesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\Slide::create([
            'title' => 'Ласкаво просимо',
            'subtitle' => 'До салону краси Mon Real!',
            'image' => 's1.png',
        ]);
        //
        \App\Slide::create([
            'title' => 'Ласкаво просимо',
            'subtitle' => 'До салону краси Mon Real!',
            'image' => 's2.png',
        ]);
        //
        \App\Slide::create([
            'title' => 'Ласкаво просимо',
            'subtitle' => 'До салону краси Mon Real!',
            'image' => 's3.png',
        ]);
    }
}
