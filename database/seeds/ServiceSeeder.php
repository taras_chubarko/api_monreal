<?php

use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\Service::create([
            'name' => 'Вечірні, випускні укладки',
            'description' => 'Lorem ipsum dolor sit amet consectetur 
adipisicing elit. Rerum exercitationem 
quae id dolorum debitis.',
            'icon' => 'hair-cutsvg',
        ]);
        \App\Service::create([
            'name' => 'Наречена під ключ',
            'description' => 'Lorem ipsum dolor sit amet consectetur 
adipisicing elit. Rerum exercitationem 
quae id dolorum debitis.',
            'icon' => 'cut.svg',
        ]);
        \App\Service::create([
            'name' => 'Нарощення волосся',
            'description' => 'Lorem ipsum dolor sit amet consectetur 
adipisicing elit. Rerum exercitationem 
quae id dolorum debitis.',
            'icon' => 'chill.svg',
        ]);
    }
}
