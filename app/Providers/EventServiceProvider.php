<?php

namespace App\Providers;

use App\Events\DeleteServiceEvent;
use App\Events\DeleteSlideEvent;
use App\Events\DeleteTeamEvent;
use App\Events\DeleteWorkEvent;
use App\Events\NewsDeleteEvent;
use App\Listeners\DeleteNewsImageListener;
use App\Listeners\DeleteServiceListener;
use App\Listeners\DeleteSlideListener;
use App\Listeners\DeleteTeamListener;
use App\Listeners\DeleteWorkListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        NewsDeleteEvent::class => [
          DeleteNewsImageListener::class,
        ],
        DeleteSlideEvent::class => [
            DeleteSlideListener::class,
        ],
        DeleteServiceEvent::class => [
            DeleteServiceListener::class,
        ],
        DeleteTeamEvent::class => [
            DeleteTeamListener::class,
        ],
        DeleteWorkEvent::class => [
            DeleteWorkListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
