<?php

namespace App\Listeners;

use App\Events\DeleteServiceEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class DeleteServiceListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(DeleteServiceEvent $event)
    {
        //
        $service = $event->service;
        Storage::delete($service->image->dirname.'/'.$service->image->basename);
    }
}
