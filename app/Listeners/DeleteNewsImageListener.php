<?php

namespace App\Listeners;

use App\Events\NewsDeleteEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class DeleteNewsImageListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(NewsDeleteEvent $event)
    {
        //
        $news = $event->news;
        Storage::delete($news->image->dirname.'/'.$news->image->basename);
    }
}
