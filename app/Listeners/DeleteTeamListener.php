<?php

namespace App\Listeners;

use App\Events\DeleteTeamEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
class DeleteTeamListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(DeleteTeamEvent $event)
    {
        //
        $team = $event->team;
        Storage::delete($team->image->dirname.'/'.$team->image->basename);
    }
}
