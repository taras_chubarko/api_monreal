<?php

namespace App\Listeners;

use App\Events\DeleteSlideEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class DeleteSlideListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(DeleteSlideEvent $event)
    {
        //
        $slide = $event->slide;
        Storage::delete($slide->image->dirname.'/'.$slide->image->basename);
    }
}
