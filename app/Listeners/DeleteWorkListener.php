<?php

namespace App\Listeners;

use App\Events\DeleteWorkEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class DeleteWorkListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(DeleteWorkEvent $event)
    {
        //
        $work = $event->work;
        Storage::delete($work->dirname.'/'.$work->basename);
    }
}
