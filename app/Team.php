<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Team extends Model
{
    //
    protected $guarded = [
        '_method',
        '_token'
    ];
    /* public function setImageAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function setImageAttribute($value)
    {
        $this->attributes['image'] = json_encode($value);
    }

    /* public function getImageAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getImageAttribute()
    {
        if( isset($this->attributes['image'])){
            return json_decode($this->attributes['image']);
        }else{
            return (object)[];
        }
    }

    /* public function getShortDescAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getShortAttribute()
    {
        return Str::limit(strip_tags($this->description), $limit = 100, $end = '...');
    }
}
