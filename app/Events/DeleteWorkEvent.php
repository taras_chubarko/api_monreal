<?php

namespace App\Events;

use App\Work;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class DeleteWorkEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $work;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Work $work)
    {
        //
        $this->work = $work;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
