<?php

namespace App\Events;

use App\Slide;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class DeleteSlideEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $slide;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Slide $slide)
    {
        //
        $this->slide = $slide;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
