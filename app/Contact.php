<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //
    protected $guarded = [
        '_method',
        '_token'
    ];

    /* public function setImageAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function setPhonesAttribute($value)
    {
        $this->attributes['phones'] = json_encode($value);
    }

    /* public function getImageAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getPhonesAttribute()
    {
        if( isset($this->attributes['phones'])){
            return json_decode($this->attributes['phones']);
        }else{
            return [];
        }
    }
}
