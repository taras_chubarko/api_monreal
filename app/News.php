<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Carbon\Carbon;
use Illuminate\Support\Str;

class News extends Model
{
    //
    use Sluggable;

    protected $guarded = [
        '_method',
        '_token'
    ];

    protected $dates = [
        'posted_at',
    ];

    /* public function sluggable
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /* public function setImageAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function setImageAttribute($value)
    {
        $this->attributes['image'] = json_encode($value);
    }

    /* public function getImageAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getImageAttribute()
    {
        if( isset($this->attributes['image'])){
            return json_decode($this->attributes['image']);
        }else{
            return (object)[];
        }
    }

    /* public function getImageAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getPostedAtAttribute()
    {
        if( isset($this->attributes['posted_at'])){
            return Carbon::parse($this->attributes['posted_at'])->format('d.m.Y');
        }else{
            return Carbon::now()->format('d.m.Y');
        }
    }

    /* public function getShortDescAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getShortAttribute()
    {
        return Str::limit(strip_tags($this->description), $limit = 200, $end = '...');
    }


}
