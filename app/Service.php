<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Cviebrock\EloquentSluggable\Sluggable;

class Service extends Model
{
    use Sluggable;
    //
    protected $guarded = [
        '_method',
        '_token'
    ];
    //
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /* public function setImageAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function setImageAttribute($value)
    {
        $this->attributes['image'] = json_encode($value);
    }

    /* public function getImageAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getImageAttribute()
    {
        if( isset($this->attributes['image'])){
            return json_decode($this->attributes['image']);
        }else{
            return (object)[];
        }
    }

    /* public function getShortDescAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getShortAttribute()
    {
        return Str::limit(strip_tags($this->description), $limit = 100, $end = '...');
    }

    public function setPricesAttribute($value)
    {
        $this->attributes['prices'] = json_encode($value);
    }

    /* public function getPricesAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getPricesAttribute()
    {
        if( isset($this->attributes['prices'])){
            return json_decode($this->attributes['prices']);
        }else{
            return (object)[];
        }
    }
}
