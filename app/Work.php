<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    //
    protected $guarded = [
        '_method',
        '_token'
    ];
}
