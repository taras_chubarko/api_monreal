<?php

namespace App\Http\Controllers;

use App\About;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class AboutController extends Controller
{
    //
    protected $fields = ['id', 'page_title', 'block_title', 'short', 'description'];
    //
    public function index(Request $request)
    {
        $items = QueryBuilder::for(About::class)
            ->allowedFields($this->fields)
            ->allowedFilters($this->fields)
            ->get();
        return $items;
    }

    /* public function save
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function save(Request $request)
    {
        About::create($request->all());
        return response()->json(true, 200);
    }

    /* public function find
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function find($id)
    {
        $items = QueryBuilder::for(About::class)
            ->allowedFields($this->fields)
            ->allowedFilters($this->fields)
            ->find($id);
        return $items;
    }

    /* public function update
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function update($id, Request $request)
    {
        About::where('id', $id)->update($request->all());
        return response()->json(true, 200);
    }
}
