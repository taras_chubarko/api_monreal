<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    /* public function auth
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function auth(Request $request)
    {
        if($request->email == config('app.ps_email') && $request->password == config('app.ps_ps')){
            return response()->json(true, 200);
        }else{
            abort(403, 'NoAccess');
        }
    }
}
