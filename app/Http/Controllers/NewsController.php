<?php

namespace App\Http\Controllers;

use App\Events\NewsDeleteEvent;
use App\News;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class NewsController extends Controller
{
    //
    protected $fields = ['id', 'title', 'image', 'description', 'status', 'slug', 'posted_at'];

    //
    public function index(Request $request)
    {
        $q = QueryBuilder::for(News::class);
        $q->allowedFields($this->fields);
        $q->allowedFilters($this->fields);
        $q->allowedSorts($this->fields);
        $q->allowedAppends(['short']);
        if($request->has('page')){
            $items = $q->paginate($request->has('limit') ? $request->limit : 20)->appends($request->query());
        }else{
            $items = $q->get();
        }
        return $items;
    }

    /* public function save
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function save(Request $request)
    {
        $item = News::create($request->all());
        $data = $this->find($item->id);
        unset($data['slug']);
        unset($data['created_at']);
        unset($data['updated_at']);
        return response()->json($data, 200);
    }

    /* public function find
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function find($id)
    {
        $items = QueryBuilder::for(News::class)
            ->allowedFields($this->fields)
            ->allowedFilters($this->fields)
            ->find($id);
        return $items;
    }

    /* public function update
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function update($id, Request $request)
    {
        News::where('id', $id)->update($request->all());
        $data = $this->find($id);
        unset($data['slug']);
        unset($data['created_at']);
        unset($data['updated_at']);
        return response()->json($data, 200);
    }

    /* public function delete
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function delete($id)
    {
       $item = News::find($id);
       event(new NewsDeleteEvent($item));
       $item->delete();
        return response()->json(true, 200);
    }

    /* public function slug
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function slug($slug)
    {
        $items = QueryBuilder::for(News::class)
            ->allowedFields($this->fields)
            ->allowedFilters($this->fields)
            ->where('slug', $slug)->first();
        return response()->json($items, 200);
    }
}
