<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class UploadController extends Controller
{
    //
    /* public function uploadImage
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function uploadImage(Request $request)
    {
        $filePath = Storage::putFile('public/'.$request->path, new File($request->image));
        return response()->json(pathinfo($filePath), 200);
    }

    /* public function removeImage
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function removeImage(Request $request)
    {
        Storage::delete($request->dirname.'/'.$request->basename);
        return response()->json(true, 200);
    }

    //
    /* public function uploadImage
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function uploadImages(Request $request)
    {
        if($request->has('images')){
            $data = [];
            foreach ($request->images as $image){
                $filePath = Storage::putFile('public/'.$request->path, new File($image));
                $data[] = pathinfo($filePath);
            }
        }
        return response()->json($data, 200);
    }
}
