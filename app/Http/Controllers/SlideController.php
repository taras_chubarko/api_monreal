<?php

namespace App\Http\Controllers;

use App\Events\DeleteSlideEvent;
use App\Slide;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class SlideController extends Controller
{
    protected $fields = ['id', 'title', 'subtitle', 'image', 'sort'];

    //
    public function index(Request $request)
    {
        $items = QueryBuilder::for(Slide::class)
            ->allowedFields($this->fields)
            ->allowedFilters($this->fields)
            ->allowedSorts($this->fields)
            ->get();
        return $items;
    }

    /* public function save
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function save(Request $request)
    {
        $item = Slide::create($request->all());
        $data = $this->find($item->id);
        unset($data['created_at']);
        unset($data['updated_at']);
        return response()->json($data, 200);
    }

    /* public function find
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function find($id)
    {
        $items = QueryBuilder::for(Slide::class)
            ->allowedFields($this->fields)
            ->allowedFilters($this->fields)
            ->find($id);
        return $items;
    }

    /* public function update
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function update($id, Request $request)
    {
        Slide::where('id', $id)->update($request->all());
        $data = $this->find($id);
        unset($data['created_at']);
        unset($data['updated_at']);
        return response()->json($data, 200);
    }

    /* public function delete
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function delete($id)
    {
        $item = Slide::find($id);
        event(new DeleteSlideEvent($item));
        $item->delete();
        return response()->json(true, 200);
    }
}
