<?php

namespace App\Http\Controllers;

use App\Events\DeleteServiceEvent;
use App\Service;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class ServiceController extends Controller
{
    protected $fields = ['id', 'name', 'description', 'image', 'sort', 'price', 'prices', 'slug'];
    //
    public function index(Request $request)
    {
        $items = QueryBuilder::for(Service::class)
            ->allowedFields($this->fields)
            ->allowedFilters($this->fields)
            ->allowedSorts($this->fields)
            ->allowedAppends(['short'])
            ->get();
        return $items;
    }

    /* public function save
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function save(Request $request)
    {
        $item = Service::create($request->all());
        $data = $this->find($item->id);
        unset($data['created_at']);
        unset($data['updated_at']);
        return response()->json($data, 200);
    }

    /* public function find
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function find($id)
    {
        $items = QueryBuilder::for(Service::class)
            ->allowedFields($this->fields)
            ->allowedFilters($this->fields)
            ->find($id);
        return $items;
    }

    /* public function update
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function update($id, Request $request)
    {
        Service::where('id', $id)->update($request->all());
        $data = $this->find($id);

        unset($data['created_at']);
        unset($data['updated_at']);
        return response()->json($data, 200);
    }

    /* public function delete
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function delete($id)
    {
        $item = Service::find($id);
        event(new DeleteServiceEvent($item));
        $item->delete();
        return response()->json(true, 200);
    }

    /* public function slug
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function slug($slug)
    {
        $items = QueryBuilder::for(Service::class)
            ->allowedFields($this->fields)
            ->allowedFilters($this->fields)
            ->where('slug', $slug)->first();
        return $items;
    }
}
