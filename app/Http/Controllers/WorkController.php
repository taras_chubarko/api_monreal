<?php

namespace App\Http\Controllers;

use App\Events\DeleteWorkEvent;
use App\Work;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class WorkController extends Controller
{
    protected $fields = ['id', 'basename', 'dirname', 'extension', 'filename', 'sort'];
    //
    public function index(Request $request)
    {
        $items = QueryBuilder::for(Work::class)
            ->allowedFields($this->fields)
            ->allowedFilters($this->fields)
            ->allowedSorts($this->fields)
            ->get();
        return $items;
    }

    /* public function find
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function find($id)
    {
        $items = QueryBuilder::for(Work::class)
            ->allowedFields($this->fields)
            ->allowedFilters($this->fields)
            ->find($id);
        return $items;
    }

    /* public function save
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function save(Request $request)
    {
        if($request->has('items')){
            foreach ($request->items as $item){
                Work::create($item);
            }
            return response()->json(true, 200);
        }
        else
        {
            return response()->json(false, 200);
        }
    }

    /* public function delete
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function delete($id)
    {
        $item = Work::find($id);
        event(new DeleteWorkEvent($item));
        $item->delete();
        return response()->json(true, 200);
    }
}
