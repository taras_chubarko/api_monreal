<?php

namespace App\Http\Controllers;

use App\Events\DeleteTeamEvent;
use App\Team;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class TeamController extends Controller
{
    protected $fields = ['id', 'name', 'description', 'image', 'sort'];
    //
    public function index(Request $request)
    {
        $items = QueryBuilder::for(Team::class)
            ->allowedFields($this->fields)
            ->allowedFilters($this->fields)
            ->allowedSorts($this->fields)
            ->allowedAppends(['short'])
            ->get();
        return $items;
    }

    /* public function save
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function save(Request $request)
    {
        $item = Team::create($request->all());
        $data = $this->find($item->id);
        unset($data['created_at']);
        unset($data['updated_at']);
        return response()->json($data, 200);
    }

    /* public function find
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function find($id)
    {
        $items = QueryBuilder::for(Team::class)
            ->allowedFields($this->fields)
            ->allowedFilters($this->fields)
            ->find($id);
        return $items;
    }

    /* public function update
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function update($id, Request $request)
    {
        Team::where('id', $id)->update($request->all());
        $data = $this->find($id);
        unset($data['created_at']);
        unset($data['updated_at']);
        return response()->json($data, 200);
    }

    /* public function delete
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function delete($id)
    {
        $item = Team::find($id);
        event(new DeleteTeamEvent($item));
        $item->delete();
        return response()->json(true, 200);
    }
}
