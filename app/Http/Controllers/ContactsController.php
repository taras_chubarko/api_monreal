<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class ContactsController extends Controller
{
    //
    protected $fields = ['id', 'email', 'phones', 'address', 'map_code', 'link_to_fb', 'link_to_inst', 'link_to_shop'];
    //
    public function index(Request $request)
    {
        $q = QueryBuilder::for(Contact::class);
        $q->allowedFields($this->fields);
        $items = $q->first();
        return response()->json($items, 200);
    }

    /* public function save
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function save(Request $request)
    {
        $item = Contact::create($request->all());
        $data = $this->find($item->id);
        unset($data['created_at']);
        unset($data['updated_at']);
        return response()->json($data, 200);
    }

    /* public function find
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function find($id)
    {
        $items = QueryBuilder::for(Contact::class)
            ->allowedFields($this->fields)
            ->allowedFilters($this->fields)
            ->find($id);
        return $items;
    }

    /* public function update
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function update($id, Request $request)
    {
        Contact::where('id', $id)->update($request->all());
        $data = $this->find($id);
        unset($data['created_at']);
        unset($data['updated_at']);
        return response()->json($data, 200);
    }
}
