<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    //
    protected $guarded = [
        '_method',
        '_token'
    ];
    /* public function setImageAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function setImageAttribute($value)
    {
        $this->attributes['image'] = json_encode($value);
    }

    /* public function getImageAttribute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getImageAttribute()
    {
        if( isset($this->attributes['image'])){
            return json_decode($this->attributes['image']);
        }else{
            return (object)[];
        }
    }
}
